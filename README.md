# Description
This in a simple image viewer.
It supports:
- jpg
- png
- bmp

With raw images it shows the embedded thumbnail:
- arw
- nef
- cr2
- orf
- raf
- rw2
- pef

# Installation
## Dependencies
imgv requires exiv2 and qt5

## Process
Change to the directory, run:
cmake -S src -B build
to initialize cmake, then run
cmake --build build
and afterwards
cmake --install
Now the viewer should be installed
