#pragma once

#include <algorithm>
#include <filesystem>
#include <vector>

#include <QApplication>
#include <QCommandLineParser>
#include <QFileDialog>
#include <QLabel>

#include "data.hpp"
#include "viewer.hpp"
#include "infopanel.hpp"
#include "helppanel.hpp"

namespace fs = std::filesystem;


/******************************************************************************
* This class contain the window of the application.
******************************************************************************/
class Window : public QWidget {
public:

	explicit Window(QWidget* parent = nullptr) : QWidget(parent) {
		setStyleSheet(
			"background-color: rgb(32, 32, 32);"
			"color: rgb(210, 210, 210);"
			"font-size: 15px;"
		);
		
		// create layout and populate it
		auto bar = new QHBoxLayout();
		bar->setContentsMargins(4, 8, 4, 8);
		name = new QLabel(this);
		name->setMinimumWidth(300);
		name->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
		bar->addWidget(name);
		bar->addStretch();
		date = new QLabel(this);
		date->setMinimumWidth(200);
		date->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		bar->addWidget(date);
		bar->addStretch();
		counter = new QLabel(this);
		counter->setMinimumWidth(300);
		counter->setAlignment(Qt::AlignHCenter | Qt::AlignRight);
		bar->addWidget(counter);

		auto photo = new QHBoxLayout();
		viewer = new Viewer(this);
		info = new InfoPanel(this);
		info->setFixedWidth(0.2 * this->width());
		info->hide();
		photo->addWidget(info);
		photo->addWidget(viewer);
		
		help = new HelpPanel(this);
		help->hide();
	
		auto layout = new QGridLayout(this);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);
		layout->addLayout(bar, 0, 0);
		layout->addLayout(photo, 1, 0);
		layout->addWidget(help, 1, 0);

		/*********************
		* keyboard shortcuts *
		**********************/

		// open new directory
		connect(new QShortcut(QKeySequence("F1"), this), &QShortcut::activated,
			[this]() {
				const auto path = QFileDialog::getOpenFileName(this, "Open",
					QDir::homePath(), "Images (*.jpg *.png *.bmp *.arw "
					"*.nef *.orf *.cr2 *.raf *.rw2 *.pef)"
				);
				if (!path.isEmpty()) open(path);
			}
		);

		// next image
		connect(new QShortcut(Qt::Key_PageDown, this), &QShortcut::activated,
			[this] () {
				if (files.empty()) return;
				
				if (index + 1 > files.size() - 1) index = 0;
				else index += 1;

				update_window();	
			}
		);

		// prev image
		connect(new QShortcut(Qt::Key_PageUp, this), &QShortcut::activated,
			[this] () {
				if (files.empty()) return;

				if ( index == 0) index = files.size() - 1;
				else index -= 1;

				update_window();	
			}
		);

		// go to first image
		connect(new QShortcut(QKeySequence(Qt::Key_Home), this), &QShortcut::activated,
			[this] () {
				if (files.empty()) return;
				
				index = 0;
				update_window();
			}
		);

		// remove current image permanently
		connect(new QShortcut(QKeySequence("Del"), this), &QShortcut::activated,
			[this] () {
				if (files.empty()) return;
				
				auto msgBox = QMessageBox(this);
				msgBox.setText("Delete current image permanently?");
				msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
				
				if (msgBox.exec() == QMessageBox::Yes) {
					QFile::remove(files.at(index).get(Data::Path));
					files.erase(files.begin() + index);
				}
				if (index >= files.size()) index--;
				update_window();
			}
		);


		// toggle infopanel
		connect(new QShortcut(QKeySequence("i"), this), &QShortcut::activated,
			[this] () { info->isHidden() ? info->show() : info->hide(); }
		);

		// fit image to window size
		connect(new QShortcut(QKeySequence("f"), this), &QShortcut::activated,
			[this] () { viewer->zoom_fit(); }
		);

		// display image at 100%
		connect(new QShortcut(QKeySequence("1"), this), &QShortcut::activated,
			[this] () { viewer->zoom_ori(); }
		);
		// zoom in
		connect(new QShortcut(QKeySequence("+"), this), &QShortcut::activated,
			[this] () { viewer->zoom_in(); }
		);

		// zoom out
		connect(new QShortcut(QKeySequence("-"), this), &QShortcut::activated,
			[this] () { viewer->zoom_out(); }
		);

		// show help
		connect (new QShortcut(QKeySequence("h"), this), &QShortcut::activated,
			[this] () { help->isHidden() ? help->show() : help->hide(); }
		);


	} // end ctor


	// read directory and open all supported images from it
	void open(const QString& path) {
		const std::vector<std::string> formats {
			".jpg", ".png", ".bmp", ".arw", ".nef", ".orf", ".cr2" , ".raf",
			".rw2", ".pef"
		};

		const bool isFile = fs::is_regular_file(fs::path(path.toStdString()));
		const auto folder = isFile ?
			fs::directory_iterator(fs::path(path.toStdString()).parent_path()) :
			fs::directory_iterator(path.toStdString())
		;
		
		// remove old files	
		files.clear();
		
		float num_available = 0; 
		std::for_each(
			//std::execution::par,
			fs::begin(folder), fs::end(folder),
			[formats, &num_available, this] (const auto& p) {
				std::string typ = p.path().extension();
				std::transform(
					typ.begin(), typ.end(),
					typ.begin(), ::tolower
				);

				if (formats.end() != std::find(formats.begin(), formats.end(), typ)) {	
					const std::string name = p.path().string();
					auto entry = ImageData(name);
					files.push_back(entry);
					if (entry.get(Data::Date_Time) != "" ) num_available += 1;
				}
			}
		);

		std::sort(files.begin(), files.end(),
			[num_available, this] (ImageData& a, ImageData& b) {
				if (num_available / files.size() > .5) {
					return a.get(Data::Date_Time_Raw) < b.get(Data::Date_Time_Raw);
				} else {
					return a.get(Data::Path) < b.get(Data::Path);	
				}
			}
		);

		if (isFile) {
			unsigned short idx = 0;
			for(auto& e : files) {
				if (e.get(Data::Path) == path) { index = idx; break; } 
				else { idx += 1; }
			}
		}
		update_window();
	}


private:
	unsigned short index = 0;
	QLabel* name = nullptr;
	QLabel* date = nullptr;
	QLabel* counter = nullptr;
	Viewer* viewer = nullptr;
	InfoPanel* info = nullptr;
	HelpPanel* help = nullptr;
	std::vector<ImageData> files;
	std::vector<QString> tags;
	std::vector<QString> filters;

	// update all labels and the viewer
	void update_window() {
		date->setText(files[index].get(Data::Date_Time));
		
		counter->setText(
			QString::number(index + 1) + " / " + QString::number(files.size())
		);
		
		auto temp = files[index].get(Data::Title);
		name->setText(
			temp == "" ? files[index].get(Data::Filename) : temp
		);
	
		info->setInfo(files[index]);
		viewer->display_image(files[index]);
	}


	void resizeEvent(QResizeEvent*) {
		info->setFixedWidth(0.2 * this->width());
	}


};
