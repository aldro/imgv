#pragma once

#include <string>
#include <sstream>

#include "exiv2/exiv2.hpp"

#include <QDateTime>
#include <QDirIterator>
#include <QFile>
#include <QValidator>
#include <QVBoxLayout>
#include <QWidget>

#include "data.hpp"

/*****************************************************************************
* This class is a widget which displays information of an image in a table.
*
* ImageInfo(): ctor
* setInfo(QString, uint width, uint height): add info from the image
*****************************************************************************/
class InfoPanel : public QWidget {

public:
	InfoPanel(QWidget* parent = nullptr) : QWidget(parent) {
		auto layout = new QVBoxLayout(this);
		lens.setWordWrap(true);
		tags.setWordWrap(true);
		title.setWordWrap(true);
		description.setWordWrap(true);
		layout->setContentsMargins(16, 16, 16, 16);
		layout->setSpacing(8);
		layout->addWidget(&date);
		layout->addWidget(&camera);
		layout->addWidget(&lens);
		layout->addWidget(&focal_length);
		layout->addWidget(&focal_length_35);
		layout->addWidget(&aperture);
		layout->addWidget(&exposure_time);
		layout->addWidget(&iso);
		layout->addWidget(&size);
		layout->addWidget(&name);
		layout->addWidget(&rating);
		layout->addWidget(&creator);
		layout->addWidget(&copyright);
		layout->addWidget(&software);
		layout->addWidget(&tags);
		layout->addWidget(&title);
		layout->addWidget(&description);
		layout->addStretch();
	}


	void setInfo(ImageData data) {
		date.setText(
			"Date/Time: " + data.get_formatted(Data::Date_Time)
		);
		camera.setText(
			"Camera: " + data.get_formatted(Data::Camera)
		);
		lens.setText(
			"Lens: " + data.get_formatted(Data::Lens)
		);
		focal_length.setText(
			"Focal Length: " + data.get_formatted(Data::Focal_Length)
		);
			focal_length_35.setText(
			"Focal Length (35mm equivalent): "
			+ data.get_formatted(Data::Focal_Length_35)
		);
		aperture.setText(
			"Aperture: " + data.get_formatted(Data::Aperture)
		);
		exposure_time.setText(
			"Exposure Time: " + data.get_formatted(Data::Exposure_Time)
		);
		iso.setText(
			"ISO: " + data.get_formatted(Data::Iso)
		);
		size.setText(
			"Image Size: " + data.get_formatted(Data::Resolution)
		);
		name.setText(
			"Name: " + data.get_formatted(Data::Filename)
		);
		rating.setText(
			"Rating: " + data.get_formatted(Data::Rating)
		);
		tags.setText(
			"Tags: " + data.get_formatted(Data::Tags)
		);
		creator.setText(
			"Creator: " + data.get_formatted(Data::Creator)
		);
		copyright.setText(
			"Copyright: " + data.get_formatted(Data::Copyright)
		);
		software.setText(
			"Software: " + data.get_formatted(Data::Software)
		);	
		title.setText(
			"Title: " + data.get_formatted(Data::Title)
		);	
		description.setText(
			"Description: " + data.get_formatted(Data::Description)
		);	
	}


private:
	ImageData* data = nullptr;
	QLabel date, camera, lens, focal_length, focal_length_35, aperture, 
	exposure_time, iso, size, name, rating, tags, creator, copyright,
	software, title, description;
};
