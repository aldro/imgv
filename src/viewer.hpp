#pragma once

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPixmap>
#include <QImageReader>
#include <QScrollArea>
#include <QScrollBar>
#include <QShortcut>
#include <QToolBar>

#include "data.hpp"

/******************************************************************************
* This class is a widget which shows a topbar and a zoomable and scrollable
* image below the bar.
*
* - Viewer(): ctor
* public slots:
* - zoom_in():
* - zoom_out():
* - zoom_fit():
* - zoom_ori():
* - display_image(): shows the current image
* signals:
* - close_viewer():
* private:
* - resiveEvent(QResizeEvent): resize widgets
******************************************************************************/
class Viewer : public QWidget {
	Q_OBJECT

public:
	explicit Viewer(QWidget* parent = nullptr) : QWidget(parent) {

		reader.setAutoTransform(true);

		// the zoomable view
		image_area = new QScrollArea(this);
		image_area->setStyleSheet("background-color: rgb(96, 96, 96);");
		container = new QLabel(this);
		container->setStyleSheet("background-color: rgba(0, 0, 0, 0);");
		container->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
		container->setScaledContents(true);
		image_area->setWidget(container);
		image_area->setVisible(false);
		image_area->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		// main layout
		auto layout = new QGridLayout(this);
		layout->setSpacing(0);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(image_area, 1, 1);
	}


public slots:

	void zoom_in() { resize(4.0 / 3.0); fit_window = false; }
	void zoom_out() { resize(3.0 / 4.0); fit_window = false; }
	void zoom_ori() { resize(1 / scalefactor); fit_window = false; } 
	void zoom_fit() {
		if (image.isNull()) return;
		zoom_ori();
		auto size = image_area->size();
		fit_window = true;
		if (image.width() < size.width() && image.height() < size.height()) {
			zoom_ori();
			return;
		}

		// substract 2 pixel because the widget frame size on each side is 1 pixel
		double w = (double(size.width() - 2)) / (image.width());
		double h = (double(size.height() - 2)) / (image.height());
		double f = w < h ? w : h;
		resize(f);
	}
	void display_image(ImageData i) {
		if (i.is_raw()) {
			image = QPixmap::fromImage(i.get_thumbnail());
		} else {
			reader.setFileName(i.get(Data::Path));
			image = QPixmap::fromImage(reader.read());
		}

		container->setPixmap(image);
		container->adjustSize();
		image_area->setVisible(true);
		zoom_fit();
	}



private:

	void resizeEvent(QResizeEvent*) { if (fit_window) zoom_fit(); }


	void resize(const double value) {
		scalefactor *= value;
		if (scalefactor > 3) scalefactor = 3;
		else if (scalefactor < 0.25 && !fit_window) scalefactor = 0.25;

		const double old_width = container->size().width();
		container->resize(scalefactor * container->pixmap(Qt::ReturnByValue).size());
		const double factor = container->size().width() / old_width;

		image_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		image_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

		// adjust scrollbar
		auto h_scrollbar = image_area->horizontalScrollBar(); 
		h_scrollbar->setValue(int(h_scrollbar->value() * factor
			+ ((factor - 1) * h_scrollbar->pageStep() / 2)
		));

		auto v_scrollbar = image_area->verticalScrollBar();
		v_scrollbar->setValue(int(v_scrollbar->value() * factor
			+ ((factor - 1) * v_scrollbar->pageStep() / 2)
		)); 
	}



private:
	QScrollArea* image_area = nullptr;
	QLabel* container = nullptr;

	QImageReader reader;
	QPixmap image;
	float scalefactor = 1.0;
	bool fit_window = true;
};
