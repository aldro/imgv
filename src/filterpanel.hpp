#pragma once

#include <QCompleter>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>


class Entry : public QWidget {

public:

	explicit Entry(QString name, QWidget* parent = nullptr) : QWidget(parent) {
		text.setText(name);
		button.setText("x");

		auto layout = new QHBoxLayout(this);
		layout->addWidget(&text);
		layout->addWidget(&button);
	}

	QPushButton button;

private:
	QLabel text;
};





class Filter_Panel : public QWidget {

public:

	explicit Filter_Panel(QWidget* parent = nullptr) : QWidget(parent) {
		

		auto layout = new QHBoxLayout(this);
		layout->setContentsMargins(16, 16, 16, 16);
		layout->setSpacing(8);
		layout->addWidget(&input);
	}


	void set_filters(QStringList tag_list) { 
		tags = tag_list;
		auto completer = new QCompleter(tag_list, this);
		completer->setCaseSensitivity(Qt::CaseInsensitive);
		input.setCompleter(completer);
	}


private:
	QLineEdit input;
	QStringList tags;
	std::vector<Entry> filters;

};
