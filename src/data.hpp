#pragma once

#include "exiv2/exiv2.hpp"
#include <QDateTime>
#include <QFileInfo>
#include <QImage>
#include <QTransform>
#include <QDebug>

enum class Data {
	Date_Time, Camera, Lens, Focal_Length, Focal_Length_35, Aperture,
	Exposure_Time, Iso, Resolution, Filename, Rating, Tags,
	Creator, Copyright, Software, Path, Date_Time_Raw,
	Title, Description, Thumbnail,
};


class ImageData {
public:

	explicit ImageData(std::string path) {
		filename = QString::fromStdString(path);
		try {
			auto data = Exiv2::ImageFactory::open(path);
			assert(data.get() != 0);
			data->readMetadata();
			exif = data->exifData();
			xmp = data->xmpData();
			width = data->pixelWidth();
			height = data->pixelHeight();
			QString extension = QFileInfo(filename).suffix().toLower();
			if ("jpg" == extension || "png" == extension || "bmp" == extension) {
				raw_photo = false;
			} else {
				raw_photo = true;
			}
			auto thumbs = Exiv2::PreviewManager(*data);
			Exiv2::PreviewPropertiesList list = thumbs.getPreviewProperties();
			if (list.size() > 0) {
				hasThumbnail = true;
				auto thumb = thumbs.getPreviewImage(list.back());
				thumbnail = QImage::fromData(thumb.pData(), thumb.size());
				QTransform t;
				t.rotate(getOrientation());
				thumbnail = thumbnail.transformed(t, Qt::SmoothTransformation);
 			} 
			available = true;
		} catch (...) {
			available = false;
		}
	}

	// get exifdata
	QString get(const Data& d) {
		if (!available) return "";

		std::stringstream result;
		result << std::setprecision(4) << std::noshowpoint;
		try {
			switch (d) {
			case Data::Date_Time:
				result << QDateTime::fromString(
					QString::fromStdString(exif["Exif.Photo.DateTimeOriginal"]
					.toString()), "yyyy:MM:dd HH:mm:ss")
					.toString("dd. MMM yyyy, hh:mm").toStdString()
				;
				break;
			
			case Data::Camera:
				result << exif["Exif.Image.Make"].toString()
					<< " " << exif["Exif.Image.Model"];
				break;

			case Data::Lens:
				result << exif["Exif.Photo.LensModel"].toString();
				break;

			case Data::Focal_Length:
				result << exif["Exif.Photo.FocalLength"].toFloat() << "mm";
				break;

			case Data::Focal_Length_35:
				result << exif["Exif.Photo.FocalLengthIn35mmFilm"].toFloat() << "mm";
				break;

			case Data::Aperture:
				result << "F" << exif["Exif.Photo.FNumber"].toFloat();
				break;

			case Data::Exposure_Time:
				result << exif["Exif.Photo.ExposureTime"].toString() << " s";
				break;

			case Data::Iso:
				result << exif["Exif.Photo.ISOSpeedRatings"].toString();
				break;
			
			case Data::Resolution:
				result << width << " x " << height;
				break;

			case Data::Filename: {
				const auto temp = filename.toStdString();
				result << temp.substr(temp.find_last_of("/\\") + 1); 
				}
				break;
			
			case Data::Rating: {
				const auto temp = xmp["Xmp.xmp.Rating"].toString();
				const auto count = temp == "" ? 0 : std::stoi(temp);
				for (int i = 0; i < count; i++) result << "★";
				}
				break;
			
			case Data::Tags:
				result << xmp["Xmp.dc.subject"].toString();
				break;	
				
			case Data::Creator:
				result << xmp["Xmp.dc.creator"].toString();
				break;

			case Data::Copyright:
				result << exif["Exif.Image.Copyright"].toString();
				break;	
				
			case Data::Software:
				result << exif["Exif.Image.Software"].toString();
				break;

			case Data::Path:
				result << filename.toStdString();
				break;
			
			case Data::Date_Time_Raw:
				result << exif["Exif.Photo.DateTimeOriginal"].toString();
				break;
			
			case Data::Title:
				result << xmp["Xmp.dc.title"].toString().erase(0, 17);
				break;

			case Data::Description:
				result << xmp["Xmp.dc.description"].toString().erase(0, 17);
				break;

			default:
				break;
			}
		} catch (...) {
			return "";
		}
		return QString::fromStdString(result.str());
	}

	QString get_formatted(Data d) {
		return "<font color='grey'>" + get(d) + "</font>";
	}

	QImage get_thumbnail() { return thumbnail; }
	bool is_raw() { return raw_photo; }

private:
	QString filename = "";
	QImage thumbnail;
	bool available = false;
	bool hasThumbnail = false;
	bool raw_photo = false;
	unsigned short width = 0;
	unsigned short height = 0;
	Exiv2::ExifData exif;
	Exiv2::XmpData xmp;

	int getOrientation() {
		long o = exif["Exif.Image.Orientation"].toInt64();
		int rotation = 0;
		switch (o) {
		case 1: rotation = 0; break;
		case 2: rotation = 0; break;
		case 3: rotation = 180; break;
		case 4: rotation = 0; break;
		case 5: rotation = 0; break;
		case 6: rotation = 90; break;
		case 7: rotation = 0; break; 
		case 8: rotation = -90; break;
		}
		return rotation;
	}
};
