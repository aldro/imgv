#pragma once

#include <QGridLayout>
#include <QLabel>
#include <QWidget>

/*****************************************************************************
* This class is a widget which displays shortcuts
*****************************************************************************/
class HelpPanel : public QWidget {

public:
	HelpPanel(QWidget* parent = nullptr) : QWidget(parent) {
		setStyleSheet(
			"background-color: rgba(32, 32, 32, 240);"
			"font-size: 18px;"
		);

		auto navigation = new QGridLayout(); 
		navigation->addWidget(new QLabel("Navigation", this), 0, 0, 1, 2, Qt::AlignHCenter);
		QFrame *line1 = new QFrame(this);
		line1->setLineWidth(2);
		line1->setMidLineWidth(1);
		line1->setFrameShape(QFrame::HLine);
		line1->setFrameShadow(QFrame::Raised);
		navigation->addWidget(line1, 1, 0, 1, 2);
		navigation->addWidget(new QLabel("Next image", this), 2, 0);
		navigation->addWidget(new QLabel("PageUp", this), 2, 1);
		navigation->addWidget(new QLabel("Previous image", this), 3, 0);
		navigation->addWidget(new QLabel("PageDown", this), 3, 1);
		navigation->addWidget(new QLabel("First image", this), 4, 0);
		navigation->addWidget(new QLabel("Pos1", this), 4, 1);		

		auto files = new QGridLayout();
		files->addWidget(new QLabel("File operation", this), 0, 0, 1, 2, Qt::AlignHCenter);
		QFrame *line2 = new QFrame(this);
		line2->setLineWidth(2);
		line2->setMidLineWidth(1);
		line2->setFrameShape(QFrame::HLine);
		line2->setFrameShadow(QFrame::Raised);
		files->addWidget(line2, 1, 0, 1, 2);	
		files->addWidget(new QLabel("Open folder", this), 2, 0);
		files->addWidget(new QLabel("F1", this), 2, 1);
		files->addWidget(new QLabel("Delete image", this), 3, 0);
		files->addWidget(new QLabel("Del", this), 3, 1);

		auto zoom = new QGridLayout();
		zoom->addWidget(new QLabel("Zooming", this), 0, 0, 1, 2, Qt::AlignHCenter);
		QFrame *line3 = new QFrame(this);
		line3->setLineWidth(2);
		line3->setMidLineWidth(1);
		line3->setFrameShape(QFrame::HLine);
		line3->setFrameShadow(QFrame::Raised);
		zoom->addWidget(line3, 1, 0, 1, 2);	
		zoom->addWidget(new QLabel("Zoom in", this), 2, 0);
		zoom->addWidget(new QLabel("+", this), 2, 1);
		zoom->addWidget(new QLabel("Zoom out", this), 3, 0);
		zoom->addWidget(new QLabel("-", this), 3, 1);
		zoom->addWidget(new QLabel("Fit to window", this), 4, 0);
		zoom->addWidget(new QLabel("f", this), 4, 1);
		zoom->addWidget(new QLabel("Zoom 100%", this), 5, 0);
		zoom->addWidget(new QLabel("1", this), 5, 1);	

		auto horizontal_layout = new QHBoxLayout();
		horizontal_layout->addStretch();
		horizontal_layout->addLayout(navigation);
		horizontal_layout->addStretch();
		horizontal_layout->addLayout(files);
		horizontal_layout->addStretch();
		horizontal_layout->addLayout(zoom);
		horizontal_layout->addStretch();

		auto vertical_layout = new QVBoxLayout(this);
		vertical_layout->addStretch();
		vertical_layout->addLayout(horizontal_layout);
		vertical_layout->addStretch();
	}



};
