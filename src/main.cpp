#include <QApplication>
#include <QCommandLineParser>

#include "window.hpp"

/******************************************************************************
* The main function who just starts the program and check the arguments given
* to it.
******************************************************************************/
int main(int argc, char* argv[]) {
	QApplication app(argc, argv);
	QCoreApplication::setApplicationName("imgv");

	QCommandLineParser p;
	p.setApplicationDescription("An image viewer");
	p.addHelpOption();
	p.addPositionalArgument("path", "path to image or folder with images");
	p.addOptions({
		{{"f", "fullscreen"}, "start window in fullscreen"}
	});

	p.process(app);

	Window w;
	w.resize(1280, 800);
	const auto args = p.positionalArguments();
	if (!args.isEmpty()) w.open(p.positionalArguments().front());

	if (p.isSet("fullscreen")) w.showFullScreen();
	else w.show();


	return app.exec();
}
